/**
 * @author JC Bailey
 * @date 20 August 2021
 */

function hasOneLowerCase (word){
    for (const letter of word){
        if (letter === letter.toLowerCase() && letter !== letter.toUpperCase()) return true;
    }
    return false
}

function hasOneUpperCase (word){
    for (const letter of word){
        if (letter === letter.toUpperCase() && letter !== letter.toLowerCase()) return true;
    }
    return false
}

function hasOneDigit (word){
    for (const letter of word){
        if (parseInt(letter)) return true;
    }
    return false
}

function hasSpecialCharacter (word){
    let sp = `!"#$%&'()*+,-./:;<=>?@[\\]^_\`{|}~`.split("")
    for (const letter of word){
        if (sp.includes(letter)) return true;
    }
    return false
}

function hasGoodLength (word){
    return word.length >= 8
}

function isInvalid(word) {
    return (word.length < 6 || word.includes(" "))
}

function passwordStrength(password) {
    if (isInvalid(password)){
        return "Invalid"
    }
    
    let score = 0
    score += hasOneLowerCase(password) ? 1 : 0 
    score += hasOneUpperCase(password) ? 1 : 0 
    score += hasOneDigit(password) ? 1 : 0 
    score += hasSpecialCharacter(password) ? 1 : 0
    score += hasGoodLength(password) ? 1 : 0

    switch(score){
        case 1:
        case 2:
            return "Weak"
        case 3:
        case 4: 
            return "Moderate"
        case 5:
            return "Strong"
    }
}