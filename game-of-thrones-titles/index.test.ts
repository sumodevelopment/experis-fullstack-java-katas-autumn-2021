import {describe, test} from "@jest/globals";
import { addPeriod, correctTitle, formatWord, isAlwaysLowerCase } from "./"

describe('Is always lower case?', ()=>{

    test('AND >> true', () =>{
        expect( isAlwaysLowerCase("AND") ).toBe( true )
    })

    test('In >> true', () =>{
        expect( isAlwaysLowerCase("In") ).toBe( true )
    })

    test('tHe >> true', () =>{
        expect( isAlwaysLowerCase("tHe") ).toBe( true )
    })

    test('Snow >> false', () =>{
        expect( isAlwaysLowerCase("Snow") ).toBe( false )
    })

    test('jazz >> false', () =>{
        expect( isAlwaysLowerCase("jazz") ).toBe( false )
    })

})

describe('Format word Upper case first', () =>{
    test('format word jOn >> Jon', ()=>{
        expect( formatWord("jOn") ).toBe("Jon")
    })
    
    test('format word SnoW >> Snow', ()=>{
        expect( formatWord("SnoW") ).toBe("Snow")
    })

    test('format word kINg >> King', ()=>{
        expect( formatWord("kINg") ).toBe("King")
    })
})

describe('Add period at end', ()=>{
    test('jOn SnoW, kINg IN thE noRth >> ...north.', () =>{
        const source = "jOn SnoW, kINg IN thE noRth"
        const expected = "jOn SnoW, kINg IN thE noRth."
        expect( addPeriod( source ) ).toBe( expected )
    })

    test('JavaScript is EaSy >> ...EaSy.', () =>{
        const source = "JavaScript is EaSy"
        const expected = "JavaScript is EaSy."
        expect( addPeriod( source ) ).toBe( expected )
    })
})

// INTEGRATION
describe('Format to correct title', () =>{
    test('jOn SnoW, kINg IN thE noRth >> Jon Snow, King in the North.', () => {
        const source = "jOn SnoW, kINg IN thE noRth"
        const expected = "Jon Snow, King in the North."
        expect( correctTitle( source ) ).toBe( expected )
    })

    test('sansa stark,lady of winterfell. >> Sansa Stark, Lady of Winterfell.', () => {
        const source = "sansa stark,lady of winterfell."
        const expected = "Sansa Stark, Lady of Winterfell."
        expect( correctTitle( source ) ).toBe( expected )
    })
})
