export function splitOnDoubleLetter(word) {

    if (!word || !word.trim()) {
        throw new Error('No word was given')
    }

    let result = word[0]

    for (let i = 1; i < word.length; i++) {
        if (word[i -1] === word[i]) {
            result += ','
        }
        result += word[i]
    }

    const splitWords = result.split(',')

    if (splitWords.length === 1) { // No double letter! 😢
        return []
    } else {
        return splitWords
    }
}
