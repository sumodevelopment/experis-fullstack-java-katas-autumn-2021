import {shuffleCount} from "./index";

describe("INT: Faro Shuffle", () => {
  test("4 Should shuffle 2 times", () => {
    const deck = 4;
    expect(shuffleCount(deck)).toBe(2);
  });

  test("8 Should shuffle 3 times", () => {
    const deck = 8;
    expect(shuffleCount(deck)).toBe(3);
  });
});